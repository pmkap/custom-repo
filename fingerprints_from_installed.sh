#!/bin/sh
# This can be used to conveniently get the certificate fingerprints of installed APKs from an Android device over adb.
# Requirements: adb and either apksigner or androguard
# Usage: ./fingerprints_from_installed.sh <app-id1> <app-id2> ...

tmp_dir="/tmp/tempapks"

if apksigner version >/dev/null 2>&1; then
    cmd="apksigner verify --print-certs \"$tmp_dir/\$i.apk\" | awk '/Signer #1.*?SHA-256/ {print \$6}'"
elif androguard --version >/dev/null 2>&1; then
    cmd="androguard sign --hash sha256 \"$tmp_dir/\$i.apk\" 2>/dev/null | awk '/sha256/ {print \$2}'"
else
    echo "Error: apksigner or androguard needs to be installed"
    exit 1
fi

mkdir -p $tmp_dir

for i in "$@"; do
    path=$(adb shell pm path "$i" | grep base.apk | cut -d: -f2)
    if ! adb pull "$path" "$tmp_dir/$i.apk" >/dev/null 2>&1; then
        echo "$i: Couldn't get apk!"
        continue
    fi

    shasum="$(eval $cmd)"

    echo "$i: $shasum"
done

rm -rf $tmp_dir
