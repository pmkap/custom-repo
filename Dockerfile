from docker.io/debian:buster-slim
RUN mkdir -p /usr/share/man/man1
RUN apt-get -qq update && apt-get -qq --no-install-recommends install \
   git \
   rsync \
   curl \
   bsdtar \
   default-jdk-headless \
   python3-pip \
   python3-setuptools \
   python3-selenium \
   firefox-esr
RUN python3 -m pip -qq install pip==21.1
RUN python3 -m pip -qq install fdroidserver==2.0.1

# install geckodriver
RUN curl -sSL "https://github.com/mozilla/geckodriver/releases/download/v0.29.1/geckodriver-v0.29.1-linux64.tar.gz" | tar -xz -C /usr/local/bin
RUN echo "641a14595f531742a9fbbba01eb2c70925f1b5b5ed86aee1e6573923cd95f43b  /usr/local/bin/geckodriver" | sha256sum -c -

# install apksigner
RUN curl -sSL "https://dl-ssl.google.com/android/repository/build-tools_r30.0.1-linux.zip" | bsdtar -xf -
RUN echo "29c1c8076f257a527929d4057d8e30a23a91b9fc72c02d3a9e40b711d3d3d553  android-11/lib/apksigner.jar" | sha256sum -c -
RUN echo "c362b2692513dbc78d29010b82ffb8dbb3a9c65f2815bcad4a4f00b291f6acb5  android-11/apksigner" | sha256sum -c -
RUN mv android-11/lib/apksigner.jar android-11/apksigner /usr/local/bin
RUN chmod +x /usr/local/bin/apksigner
